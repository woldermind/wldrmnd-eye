#INSTALLATION
1. Mark "src" as source-root
2. Set project SDK & project Language Level to 1.8
3. Run

##Optional (in case of maven error) 
To install external OCR-library (to reject some errors, just copy/paste)
* mvn install:install-file -Dfile=lib/com.asprice/java-ocr-api.jar -DgroupId=com.asprice -DartifactId=ocr -Dversion=1.0 -Dpackaging=jar
* Reload all maven projects

Install the **Lombok Plugin** in your IDEA or Eclipse, otherwise the editor would come up with false alarms.


##Here are some basic functions:

* Copy / Cut and paste
* Delete
* Rename
* Check out the attributes
* Multiple Selection

By double-clicking the thumbnail, it will show the picture in a separate window with some features below:

* Zoom in / Zoom out
* Switch pictures
* Slide show
 
 I also maintain some creative features in this app for you :

* OCR text recognition
* Picture compression
* History records
* Picture stitching
* Sorting
* More...

> Note: Remember to install the **Lombok Plugin** when using IDEA or Eclipse, otherwise the editor would come up with false alarms.


