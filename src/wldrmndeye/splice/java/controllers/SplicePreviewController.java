package wldrmndeye.splice.java.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.controllers.HomeController;
import wldrmndeye.home.java.model.ImageModel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class SplicePreviewController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private JFXButton saveButton;
    @FXML
    private ImageView imageView;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private VBox vBox;

    private ImageModel imageModel;

    private ArrayList<ImageModel> imageModelList;
    private HomeController hc;
    private JFXSnackbar snackbar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(0);

        ControllerUtil.controllers.put(this.getClass().getSimpleName(), this);
        hc = (HomeController) ControllerUtil.controllers.get(
                HomeController.class.getSimpleName());

        saveButton.translateYProperty().bind(
                rootPane.heightProperty().divide(15).multiply(5));
        saveButton.translateXProperty().bind(
                rootPane.widthProperty().divide(15).multiply(6));
        snackbar = new JFXSnackbar(hc.getRootPane());
    }

    public void setImageModelList(ArrayList<ImageModel> set) {
        this.imageModelList = set;
        scrollPane.setContent(vBox);
        int number = 0;

        for (ImageModel im : imageModelList) {
            Image image = new Image(im.getImageFile().toURI().toString());
            ImageView imageView = new ImageView(image);
            if (number == 0) {
                this.imageModel = im;
                this.imageView = imageView;
                number++;
            }
            imageView.setSmooth(true);
            imageView.setFitWidth(800);
            imageView.setPreserveRatio(true);
            imageView.setStyle("-fx-margin:0;-fx-padding:0;");
            vBox.getChildren().add(imageView);
        }
    }

    @FXML
    private void snap() {
        WritableImage wa = imageView.getParent().snapshot(null, null);
        // Set the picture name to include the current system time
        Date date = new Date();
        Stage stage = 
                (Stage) imageView.getScene().getWindow();
        SimpleDateFormat dateFormat = 
                new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        String prefix = 
                imageModelList.get(0).getImageNameWithoutExtension();

        try {
            BufferedImage buff = SwingFXUtils.fromFXImage(wa, null);
            System.out.println("buff = " + buff);
            ImageIO.write(
                    buff, 
                    "png",
                    new File(imageModel.getImageParentPath() + "\\" +
                            prefix +
                            "_more_" +
                            dateFormat.format(date) +
                            ".png"));
            hc.sortAndRefreshImagesList(hc.getSortComboBox().getValue());
            stage.close();
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent(
                            "The stitching is complete" +
                            " and a copy has been created"));
        } catch (IOException e) {
            stage.close();
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent(
                            "Stitching failed, " +
                            "the picture may be too long"));
            e.printStackTrace();
        }
    }

}
