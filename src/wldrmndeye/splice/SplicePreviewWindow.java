package wldrmndeye.splice;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import wldrmndeye.home.java.model.ImageModel;
import wldrmndeye.splice.java.controllers.SplicePreviewController;

import java.io.IOException;
import java.util.ArrayList;

public class SplicePreviewWindow extends Application {

    public static double windowWidth;
    public static double windowHeight;
    private SplicePreviewController sp;
    private ArrayList<ImageModel> imageModelList;

    @Override
    public void init() throws Exception {
        super.init();
        adaptWindowSizeAccordingScreenSize();
    }

    private void adaptWindowSizeAccordingScreenSize() {
        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            windowWidth = bounds.getWidth() / 1.5;
            windowHeight = bounds.getHeight() / 1.5;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/wldrmndeye/" +
                        "splice/resources/fxml/SplicePreview.fxml"));

        Parent root = null;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(
                new JFXDecorator(stage, root),
                windowWidth,
                windowHeight);

        // Obtain the controller instance of the window through FXMLLoader
        sp = fxmlLoader.getController();
        sp.setImageModelList(imageModelList);

        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(
                this.getClass().getResource(
                "/wldrmndeye/" +
                        "splice/resources/css/splice.css")
                .toExternalForm());

        stage.setTitle("Picture stitching preview");
        stage.getIcons().add(new Image(
                this.getClass().getResourceAsStream(
                        "/wldrmndeye/" +
                                "home/resources/icons/app.png")));
        stage.setScene(scene);
        stage.show();
    }


    public void initImageList(ArrayList<ImageModel> imageModelList){
        this.imageModelList = imageModelList;
    }

}
