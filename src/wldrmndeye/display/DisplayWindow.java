package wldrmndeye.display;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.java.controllers.DisplayWindowController;
import wldrmndeye.home.java.model.ImageModel;

import java.io.IOException;

public class DisplayWindow extends Application {

    private static final Logger LOG =
            LoggerFactory.getLogger(DisplayWindow.class);
    
    public static double windowWidth = 800;
    public static double windowHeight = 600;
    
    @Getter
    private static Stage stage;


    private ImageModel initImage;
    protected DisplayWindowController displayWindowController;

    public DisplayWindow() {}

    @Override
    public void init() throws Exception {
        super.init();
        adaptWindowSizeAccordingScreenSize();
        LOG.info("DisplayWindow initialization...");
    }

    private void adaptWindowSizeAccordingScreenSize() {
        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            windowWidth = bounds.getWidth() / 1.5;
            windowHeight = bounds.getHeight() / 1.5;
        } catch (Exception e) {
            LOG.error("Can't adapt window size according screen size");
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) {
        DisplayWindow.stage = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                        "/wldrmndeye/" +
                                "display/resources/fxml/DisplayWindow.fxml"));
        Parent root = null;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            LOG.error("Can't load DisplayWindow resources file");
            e.printStackTrace();
        }

        Scene scene = null;
        if (root != null) {
            scene = new Scene(
                    new JFXDecorator(stage, root), windowWidth, windowHeight);
            
            ObservableList<String> stylesheets = scene.getStylesheets();
            stylesheets.addAll(this.getClass().getResource(
                    "/wldrmndeye/" +
                            "display/resources/css/display.css")
                    .toExternalForm());
        }

        //Window property settings
        stage.setTitle(initImage.getImageName());
        stage.getIcons().add(
                new Image(
                        this.getClass().getResourceAsStream(
                                "/wldrmndeye/" +
                                        "home/resources/icons/app.png")));
        stage.setScene(scene);

        //Obtain the controller instance of the display window through FXMLLoader
        displayWindowController = fxmlLoader.getController();  
        displayWindowController.initImage(initImage);

        stage.show();
        LOG.info("Starting a new wldrmndeye:display window...");
    }

    public void setImage(ImageModel im) {
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.initImage = im;
    }
}
