package wldrmndeye.display.java.model;

import com.jfoenix.controls.JFXSnackbar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.java.controllers.DisplayWindowController;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.model.ImageModel;

import java.util.ArrayList;

public class ImageSwitcher {

    private static final Logger LOG =
            LoggerFactory.getLogger(ImageSwitcher.class);
    
    private ArrayList<ImageModel> images;

    protected JFXSnackbar snackbar;
    protected DisplayWindowController displayWindowController;

    public ImageSwitcher() {
        displayWindowController =
                (DisplayWindowController) ControllerUtil.controllers.get(
                        DisplayWindowController.class.getSimpleName());
        
        snackbar = new JFXSnackbar(displayWindowController.getRootPane());
        LOG.info("Image switcher loading...");
    }

    public ImageSwitcher(ArrayList<ImageModel> images) {
        this();
        this.images = images;
    }

    public ImageModel nextImage(ImageModel im) {
        int i;
        for (i = 0; i < images.size(); i++) {
            if (images.get(i).getImageName().equals(im.getImageName())) {
                if (i == images.size() - 1) {
                    LOG.info("The last one has been reached, " +
                            "the first one is being viewed");
                    snackbar.enqueue(
                            new JFXSnackbar.SnackbarEvent(
                                    "Viewing the first picture"));
                }

                break;
            }
        }
        return images.get((i + 1) % (images.size()));

    }

    public ImageModel lastImage(ImageModel im) {
        int i;
        for (i = 0; i < images.size(); i++) {
            if (images.get(i).getImageName().equals(im.getImageName())) {
                if (i == 0) {
                    LOG.info("The first photo has been reached," +
                            " the last one is being viewed");
                    snackbar.enqueue(
                            new JFXSnackbar.SnackbarEvent("Viewing the last one"));
                    i = images.size();
                }
                break;
            }
        }
        return images.get((i - 1) % (images.size()));

    }
}
