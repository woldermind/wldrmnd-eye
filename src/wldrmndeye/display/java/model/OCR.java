package wldrmndeye.display.java.model;

import com.asprise.ocr.Ocr;
import com.asprise.ocr.OcrException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class OCR {

    private static final Logger LOG =
            LoggerFactory.getLogger(OCR.class);
    
    public static String doOcr(String imagePath) {

        String beforeName = imagePath.substring(0, imagePath.lastIndexOf("."));
        String afterName = imagePath.substring(imagePath.lastIndexOf("."));
        String newImagePath = beforeName + "_only" + afterName;
        File image = new File(newImagePath);

        String result;

        if (image.exists()) {
            result = OCR(newImagePath);
        } else {
            result = OCR(imagePath);
        }
        
        if (result == null || result.isEmpty()) {
            result = "No identifiable text in the picture";
            LOG.warn("OCR Result: No identifiable text in the picture");
        }

        return result;
    }
    
    private static String OCR(String filePath) {
        try {
            Ocr.setUp();
            Ocr ocr = new Ocr();
            ocr.startEngine("eng", Ocr.SPEED_FASTEST);
            String recognizedText = ocr.recognize(
                    new File[]{
                            new File(filePath)
                    },
                    Ocr.RECOGNIZE_TYPE_ALL, 
                    Ocr.OUTPUT_FORMAT_PLAINTEXT); 
            ocr.stopEngine();
            return recognizedText;
        } catch (OcrException e) {
            LOG.error("Something goes wrong with OCR Recognition");
        }
        return null;
    }
}
