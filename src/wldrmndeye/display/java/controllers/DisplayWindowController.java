package wldrmndeye.display.java.controllers;

import com.jfoenix.controls.JFXSnackbar;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.DisplayWindow;
import wldrmndeye.display.java.model.ImageSwitcher;
import wldrmndeye.display.java.model.OCR;
import wldrmndeye.home.java.components.CustomDialog;
import wldrmndeye.home.java.components.DialogType;
import wldrmndeye.home.java.controllers.AbstractController;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.controllers.HomeController;
import wldrmndeye.home.java.model.ImageListModel;
import wldrmndeye.home.java.model.ImageModel;
import wldrmndeye.home.java.model.SelectedModel;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class DisplayWindowController extends AbstractController implements Initializable {

    private static final Logger LOG =
            LoggerFactory.getLogger(DisplayWindowController.class);

    @FXML
    @Getter
    public StackPane rootPane;
    public HBox toolbar;
    private Stage stage;

    @FXML
    @Setter
    @Getter
    private ImageView imageView;

    @Setter
    @Getter
    private Image image;
    private ImageModel imageModel;
    public ArrayList<ImageModel> imageModelArrayList;

    @Getter
    private JFXSnackbar snackbar;
    private ImageSwitcher imageSwitcher;
    private HomeController homeController;

    public DisplayWindowController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ControllerUtil.controllers.put(this.getClass().getSimpleName(), this);
        homeController = (HomeController) ControllerUtil.controllers.get(
                HomeController.class.getSimpleName());

        toolbar.translateYProperty().bind(
                rootPane.heightProperty().divide(5).multiply(2));
        snackbar = new JFXSnackbar(rootPane);
        stage = DisplayWindow.getStage();

        LOG.info("Display window initialization done...");
    }

    public void initImage(ImageModel newImageModel) {

        if (newImageModel == null) {
            this.imageModel = null;
            imageView.setImage(null);
            return;
        }

        if (homeController.isComboBoxClicked()) {
            imageModelArrayList = ImageListModel.refreshList(
                    newImageModel.getImageFile().getParent(),
                    homeController.getSortComboBox().getValue());
        } else {
            imageModelArrayList = refreshImageModelList(newImageModel);
        }

        this.imageModel = newImageModel;
        this.image = new Image(newImageModel.getImageFile().toURI().toString());
        this.imageView.setImage(image);
        this.imageSwitcher = new ImageSwitcher(imageModelArrayList);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);

        // The following adapts to the proportion of the picture to avoid
        // excessively large width and incomplete display
        // Get the current window height ratio
        double winWidth, winHeight;
        if (stage != null) {
            winWidth = stage.getScene().getWidth();
            winHeight = stage.getScene().getHeight();
        } else {
            winWidth = DisplayWindow.windowWidth;
            winHeight = DisplayWindow.windowHeight;
        }
        double windowAspectRatio = winWidth / winHeight;
        double pictureAspectRatio = image.getWidth() / image.getHeight();

        clearDataAfterLastSetting();

        if (isImageSizeGreaterThanWindowSize(winWidth, winHeight)) {
            if (pictureAspectRatio > windowAspectRatio) {
                imageView.fitWidthProperty().bind(rootPane.widthProperty());
            } else {
                imageView.fitHeightProperty().bind(rootPane.heightProperty());
            }
        } else {
            imageView.fitWidthProperty().setValue(image.getWidth());
        }

        setImageMouseAction();
    }

    private boolean isImageSizeGreaterThanWindowSize(
            double winWidth,
            double winHeight) {
        return image.getWidth() > winWidth || image.getHeight() > winHeight;
    }

    private void clearDataAfterLastSetting() {
        imageView.fitWidthProperty().unbind();
        imageView.fitHeightProperty().unbind();
        imageView.setFitHeight(0);
        imageView.setFitWidth(0);
    }

    private void setImageMouseAction() {
        imageView.setOnScroll(this::zoomInAndOut);

        final double[] lastPosition = new double[2];
        imageView.setOnMousePressed(event -> {
            lastPosition[0] = event.getX();
            lastPosition[1] = event.getY();
        });

        imageView.setOnMouseDragged(event -> {
            Translate tran = new Translate(
                    event.getX() - lastPosition[0],
                    event.getY() - lastPosition[1]);
            imageView.getTransforms().add(tran);
        });
    }

    private void zoomInAndOut(javafx.scene.input.ScrollEvent event) {
        if (event.getDeltaY() < 0) {
            Scale scale = new Scale(0.9, 0.9,
                    event.getX(), event.getY());
            imageView.getTransforms().add(scale);
        }
        if (event.getDeltaY() > 0) {
            Scale scale = new Scale(1.1, 1.1,
                    event.getX(), event.getY());
            imageView.getTransforms().add(scale);
        }
    }

    @FXML
    private void restoreInitialZoomRatioAndPosition() {
        imageView.setScaleX(1.0);
        imageView.setScaleY(1.0);
        imageView.getTransforms().clear();
    }

    //-------------toolbar button event---------------
    @FXML
    private void zoomIn() {
        imageView.setScaleX(imageView.getScaleX() * 1.25);
        imageView.setScaleY(imageView.getScaleY() * 1.25);
    }

    @FXML
    private void zoomOut() {
        imageView.setScaleX(imageView.getScaleX() * 0.75);
        imageView.setScaleY(imageView.getScaleY() * 0.75);
    }

    @FXML
    private void showPreviousImg() throws IOException {
        restoreInitialZoomRatioAndPosition();

        if (imageModel != null)
            imageModelArrayList = refreshImageModelList(imageModel);

        if (isImageArrayListEmpty()) {
            showPictureFolderEmpty();
            initImage(null);
            setNoPictureTitle();
        } else {
            initImage(imageSwitcher.lastImage(imageModel));
            stage.setTitle(imageModel.getImageName());
        }
    }

    private ArrayList<ImageModel> refreshImageModelList(ImageModel imageModel) {
        return ImageListModel.refreshList(imageModel.getImageFile().getParent());
    }

    @FXML
    public void showNextImg() throws IOException {
        restoreInitialZoomRatioAndPosition();

        if (imageModel != null)
            imageModelArrayList = refreshImageModelList(imageModel);

        if (isImageArrayListEmpty()) {
            showPictureFolderEmpty();
            this.imageModel = null;
            imageView.setImage(null);
            setNoPictureTitle();
        } else {
            initImage(imageSwitcher.nextImage(imageModel));
            stage.setTitle(imageModel.getImageName());
        }
    }

    private void setNoPictureTitle() {
        stage.setTitle("No picture");
    }

    private void showPictureFolderEmpty() {
        snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent("This folder picture is empty"));
    }

    private boolean isImageArrayListEmpty() {
        return imageModelArrayList == null || imageModelArrayList.size() == 0;
    }

    @FXML
    private void playSlide() {
        restoreInitialZoomRatioAndPosition();  
        toolbar.setVisible(false);
        stage.setFullScreen(true);
        snackbar.enqueue(new JFXSnackbar.SnackbarEvent(
                "Start the slide show, click any key to end"));

        TimerTask timerToPageTurn = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    try {
                        showNextImg();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        };
        Timer timer = new Timer();
        long startWaitingTine = 5000; 
        long executionInterval = 5000; 
        timer.scheduleAtFixedRate(
                timerToPageTurn, 
                startWaitingTine,
                executionInterval); 

        imageView.setOnMouseClicked(event -> stopSlide(timer, stage));

        imageView.getScene().setOnKeyPressed(event -> stopSlide(timer, stage));
    }

    private void stopSlide(Timer timer, Stage stage) {
        timer.cancel();
        toolbar.setVisible(true);
        stage.setFullScreen(false);
        stage.sizeToScene();
        snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent("Slide show end"));

        resetImageViewEvents();
    }

    private void resetImageViewEvents() {
        imageView.getScene().setOnKeyPressed(event -> {
        });
        imageView.setOnMouseClicked(event -> {
        });
    }

    @FXML
    private void ocr() {
        CustomDialog loading = new CustomDialog(
                this,
                DialogType.INFO,
                imageModel,
                "Processing");
        loading.setLoadingSpinner();
        loading.show();
        LOG.info("OCR Processing...");
        
        Task ocrTask = getOCRTaskAndCloseLoading(loading);
        ocrTask.messageProperty().addListener((observable, oldValue, newValue) -> {
            CustomDialog dialog = new CustomDialog(this,
                    DialogType.INFO,
                    imageModel,
                    "Recognition result");
            dialog.setBodyLabel(newValue);
            dialog.show();
            LOG.info("Recognition success");
        });
        new Thread(ocrTask).start(); 
    }

    private Task getOCRTaskAndCloseLoading(CustomDialog loading) {
        return new Task() {
            @Override
            protected Object call() {
                String path = imageModel.getImageAbsoluteFilePath();
                File file = new File(path);
                if (!file.exists()) {
                    LOG.error("Image does not exist!");
                }
                String result = OCR.doOcr(path);
                loading.close();
                updateMessage(result);
                return true;
            }
        };
    }

    @FXML
    public void showInfo() {
        if (imageModel == null) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("No attribute display"));
            LOG.warn("No info attribute display");
            return;
        }
        Image image = new Image(imageModel.getImageFile().toURI().toString());
        new CustomDialog(this, DialogType.INFO, imageModel,
                imageModel.getImageName(),
                appendedImageInfo(image, imageModel).toString()).show();
    }

    public static StringBuilder appendedImageInfo(Image image,
                                                  ImageModel imageModel) {
        StringBuilder info = new StringBuilder();
        for (StringBuilder stringBuilder :
                Arrays.asList(
                        info.append("size：").
                                append(image.getWidth()).
                                append(" × ").
                                append(image.getHeight()),
                        info.append("types of：").
                                append(imageModel.getImageType().toUpperCase()),
                        info.append("format size：").
                                append(imageModel.getFormatSize()),
                        info.append("date：").
                                append(imageModel.getFormatTime()))) {
            stringBuilder.append("\n");
        }
        info.append("\nposition：").append(imageModel.getImageAbsoluteFilePath());
        return info;
    }

    @FXML
    private void delete() {
        if (imageModel == null) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("No files to delete"));
            LOG.warn("No files to delete");
            return;
        }
        SelectedModel.setSourcePath(imageModel);
        new CustomDialog(this,
                DialogType.DELETE,
                imageModel,
                "Delete picture",
                "Delete files: " +
                        imageModel.getImageName() +
                        "\n\nYou can find it in the recycle bin.")
                .show();
    }

    @FXML
    private void fullScreen() {
        try {
            if (stage.isFullScreen()) {
                stage.setFullScreen(false);
                stage.sizeToScene();
            } else {
                stage.setFullScreen(true);
            }
        } catch (Exception e) {
            LOG.error("Can't produce full screen");
        }
    }

    @FXML
    private void compress() {
        SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
        int success = SelectedModel.compressImage(800);
        if (success != 0) {
            initImage(imageModel);
            snackbar.enqueue(new JFXSnackbar.SnackbarEvent(
                            "Compressed" + success + "Pictures and create a copy"));
            LOG.info("Compression success");
            try {
                refreshThumbnailList();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            snackbar.enqueue(new JFXSnackbar.SnackbarEvent(
                            "No image compression. " + 
                                    "Compression conditions: greater than 800KB"));
            LOG.warn("Compression failed: image must be greater than 800KB");
        }
    }

    private void refreshThumbnailList() throws IOException {
        homeController.placeImages(ImageListModel.initImageList(
                imageModel.getImageParentPath()),
                imageModel.getImageParentPath());
    }
}