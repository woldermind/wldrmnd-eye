package wldrmndeye.home.java.components;

public enum DialogType {
    RENAME,     
    DELETE,     
    REPLACE,    
    INFO,       
}
