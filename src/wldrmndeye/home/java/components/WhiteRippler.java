package wldrmndeye.home.java.components;

import com.jfoenix.controls.JFXRippler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * Inherited from {@link JFXRippler}, set a specific style *
 * @see com.jfoenix.controls.JFXRippler
 **/

public class WhiteRippler extends JFXRippler {

    public WhiteRippler(Node control) {
        super(control);
        super.setRipplerFill(Color.WHITE);
        setAlignment(Pos.BOTTOM_CENTER);
        setPrefHeight(170);
        setPrefWidth(170);
    }
    
}
