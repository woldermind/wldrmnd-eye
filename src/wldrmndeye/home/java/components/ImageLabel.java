package wldrmndeye.home.java.components;

import javafx.scene.control.Label;
import javafx.scene.text.Font;

import static javafx.geometry.Pos.CENTER;

/**
 * Picture file name tag, add specific style
 */
public class ImageLabel extends Label {

    public ImageLabel(String text) {
        super(text);
        setAlignment(CENTER);
        setFont(Font.font(16));
        setStyle("-fx-padding:15 0 0 0;");
    }

}
