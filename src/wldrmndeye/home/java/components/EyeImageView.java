package wldrmndeye.home.java.components;

import com.jfoenix.effects.JFXDepthManager;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * ImageView inherited from JavaFX.
 * By default, use JFXDepthManager to add projection.
 * */
public class EyeImageView extends ImageView {
    
    public EyeImageView(Image image) {
        super(image);
        setImageDepth();
        setPreserveRatio(true);
        setFitHeight(150);
        setFitWidth(150);
    }

    private void setImageDepth() {
        JFXDepthManager.setDepth(this, 1);
    }
    
}
