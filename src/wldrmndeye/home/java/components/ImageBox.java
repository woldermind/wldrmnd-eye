package wldrmndeye.home.java.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.effects.JFXDepthManager;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.DisplayWindow;
import wldrmndeye.home.java.controllers.PopupMenuController;
import wldrmndeye.home.java.model.ImageModel;
import wldrmndeye.home.java.model.SelectionModel;

import java.io.IOException;


/**
 * Thumbnail unit of the main window.
 * Used to wrap the image {@link EyeImageView} and 
 * the image file name {@link ImageLabel}.
 * Inherited from {@link VBox}, add specific styles.
 */
@Getter
@Setter
public class ImageBox extends VBox {

    private static final Logger LOG =
            LoggerFactory.getLogger(ImageBox.class);
    
    private ImageModel imageModel;
    private EyeImageView eyeImageView;
    private JFXPopup popUpMenu;
    @Getter
    private JFXCheckBox checkBox = new JFXCheckBox();
    
    
    public ImageBox(ImageModel imageModel) {
        this.imageModel = imageModel;
        EyeImageView imageView = new EyeImageView(
                new Image(imageModel.getImageFile().toURI().toString(),
                100,
                100,
                true,
                true,
                true));
        this.eyeImageView = imageView;
        WhiteRippler waterRippleClickEffect = new WhiteRippler(imageView);
        ImageLabel imageFileNameLabel = new ImageLabel(imageModel.getImageName());
        imageFileNameLabel.setStyle("-fx-padding:7 0 7 -2;");

        HBox hBox = new HBox(checkBox, imageFileNameLabel);
        hBox.setAlignment(Pos.CENTER);
        hBox.setStyle("-fx-padding:5 5 3 5;");

        getChildren().addAll(waterRippleClickEffect, hBox);
        setMaxSize(170, 170);
        setAlignment(Pos.BOTTOM_CENTER);

        String fileInformationTips = String.format(
                "Name: %s\n" +
                "Size: %s",
                imageModel.getImageName(),
                imageModel.getFormatSize());
        Tooltip.install(this, new Tooltip(fileInformationTips));

        JFXDepthManager.setDepth(this, 0);
        imageFeedbackOnMouseEvents();
        initPopUpMenu();
        initCheckBox();
    }
    
    ImageBox imageBox = this;

    private void initCheckBox() {
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                SelectionModel.add(imageBox);
            } else {
                SelectionModel.remove(imageBox);
            }
        });
    }
    
    private void initPopUpMenu() {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/wldrmndeye/" +
                        "home/resources/fxml/PopupMenu.fxml"));
        loader.setController(new PopupMenuController(this));
        try {
            popUpMenu = new JFXPopup(loader.load());
        } catch (IOException e) {
            LOG.error("Pop Up Menu does not response");
            e.printStackTrace();
        }
        popUpMenu.setAutoHide(true);
    }

    private void imageFeedbackOnMouseEvents() {
        setOnMouseClicked(event -> {
            if (isLeftMouseButtonDoubleClicked(event)) {
                DisplayWindow dw = new DisplayWindow();
                dw.setImage(imageModel);
                dw.start(new Stage());
            } else if (isRightMouseButtonClicked(event)) {
                initPopUpMenu();
                popUpMenu.show(this,
                        JFXPopup.PopupVPosition.TOP,
                        JFXPopup.PopupHPosition.LEFT,
                        100, 100);
            }
        });

        this.setOnMouseMoved(event -> {
            this.setStyle("-fx-background-color:rgba(0, 0, 0, 0.07);");
        });

        this.setOnMouseExited(event -> {
            this.setStyle("-fx-background-color:transparent;");
        });

    }

    private boolean isRightMouseButtonClicked(MouseEvent event) {
        return event.getButton() == MouseButton.SECONDARY;
    }

    private boolean isLeftMouseButtonDoubleClicked(javafx.scene.input.MouseEvent event) {
        return event.getButton() == MouseButton.PRIMARY &&
                event.getClickCount() == 2;
    }
    
}
