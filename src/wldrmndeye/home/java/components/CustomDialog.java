package wldrmndeye.home.java.components;


import com.jfoenix.controls.*;
import javafx.scene.Node;
import javafx.scene.control.Label;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.java.controllers.DisplayWindowController;
import wldrmndeye.home.java.controllers.AbstractController;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.controllers.HomeController;
import wldrmndeye.home.java.model.ImageModel;
import wldrmndeye.home.java.model.SelectedModel;

import java.io.IOException;

/**
 * Customized and reusable dialog boxes,
 * reducing repetitive code when using dialog boxes
 */
public class CustomDialog extends JFXDialog {

    private static final Logger LOG =
            LoggerFactory.getLogger(CustomDialog.class);
    
    @Setter
    private ImageModel targetImage;

    private AbstractController controller;
    private HomeController homeController =
            (HomeController) ControllerUtil.controllers.get(
                    HomeController.class.getSimpleName());
    private DisplayWindowController displayWindowController =
            (DisplayWindowController) ControllerUtil.controllers.get(
                    DisplayWindowController.class.getSimpleName());

    private DialogType type;

    @Getter
    private JFXButton leftButton;
    @Getter
    private JFXButton rightButton;

    @Getter
    private Label headingLabel;
    @Getter
    private Label bodyLabel;

    @Getter
    private JFXTextArea bodyTextArea;
    private JFXTextField bodyTextField;

    @Getter
    private JFXDialogLayout layout = new JFXDialogLayout();

    /**
     * @param controller  The controller of the interface where the dialog appears
     *                    Such as: need to pop up on the main interface,
     *                    then pass in {@link HomeController}
     * @param type        Types of dialog boxes, see {@link DialogType}
     * @param targetImage The target image object to be processed
     */
    public CustomDialog(AbstractController controller,
                        DialogType type,
                        ImageModel targetImage) {
        this.controller = controller;
        this.type = type;
        this.targetImage = targetImage;

        leftButtonSetup();
        rightButtonSetup();

        this.setOverlayClose(true);
        layout.setMaxWidth(500);

        switch (type) {
            case INFO:
                makeInfoDialog();
                break;
            case DELETE:
                makeDeleteDialog();
                break;
            case RENAME:
                makeRenameDialog();
                break;
            case REPLACE:
                makeReplaceDialog();
                break;
            default:
        }
        
        LOG.info("Custom Dialog initialization...");
    }

    private void rightButtonSetup() {
        rightButton = new JFXButton();
        rightButton.getStyleClass().add("dialog-confirm");
        rightButton.setText("Confirm");
        setCloseAction(rightButton);
    }

    private void leftButtonSetup() {
        leftButton = new JFXButton();
        leftButton.getStyleClass().add("dialog-cancel");
        leftButton.setText("Cancel");
        setCloseAction(leftButton);
    }

    /**
     * @param controller    The controller of the interface where the dialog box appears
     *                      If you need to pop up on the main interface, pass in the
     *                      instance of {@link HomeController}
     * @param type          Dialog type, see {@link DialogType} for details
     * @param targetImage   The target image object to be processed
     * @param headingText   dialog title
     */
    public CustomDialog(AbstractController controller,
                        DialogType type,
                        ImageModel targetImage,
                        String headingText) {
        this(controller, type, targetImage);
        setHeadingLabel(headingText);
    }

    /**
     * @param controller    The controller of the interface where the dialog box appears
     *                      If you need to pop up on the main interface, pass in the
     *                      instance of {@link HomeController}
     * @param type          Dialog type, see {@link DialogType} for details
     * @param targetImage   The target image object to be processed
     * @param headingText   dialog title
     * @param bodyText      body
     */
    public CustomDialog(AbstractController controller,
                        DialogType type, ImageModel targetImage,
                        String headingText, String bodyText) {
        this(controller, type, targetImage, headingText);
        setBodyLabel(bodyText);
    }
    
    public void setHeadingLabel(String headingText) {
        headingLabel = new Label(headingText);
        headingLabel.getStyleClass().add("dialog-heading");
        layout.setHeading(headingLabel);
    }

    public void setBodyLabel(String bodyText) {
        bodyLabel = new Label(bodyText);
        bodyLabel.getStyleClass().add("dialog-body");
        if (type == DialogType.INFO) {
            setBodyTextArea(bodyText);
        } else {
            layout.setBody(bodyLabel);
        }
    }
    
    public void setBodyContent(Node... body) {
        layout.setBody(body);
    }
    
    @Override
    public void show() {
        if (leftButton != null && rightButton != null) {
            layout.setActions(leftButton, rightButton);
        } else {
            LOG.error("Dialog button not specified");
        }
        this.setContent(layout);
        this.show(controller.getRootPane());
        LOG.info("Showing Custom Dialog");
    }

    private void setBodyTextArea(String text) {
        bodyTextArea = new JFXTextArea(text);
        bodyTextArea.getStyleClass().addAll(
                "dialog-text-area",
                "dialog-body");
        bodyTextArea.setEditable(false);
        layout.setBody(bodyTextArea);
    }

    private void setBodyTextField() {
        bodyTextField = new JFXTextField();
        bodyTextField.setText(targetImage.getImageName());
        bodyTextField.getStyleClass().addAll(
                "rename-text-field",
                "dialog-body");
        layout.setBody(bodyTextField);
    }

    private void setCloseAction(JFXButton button) {
        button.setOnAction(event -> {
            this.close();
        });
    }

    public void setLoadingSpinner() {
        JFXSpinner spinner = new JFXSpinner(-1);
        layout.setBody(spinner);
    }

    private void makeDeleteDialog() {
        rightButton.setText("Delete");
        rightButton.setStyle("-fx-text-fill: RED;");
        rightButton.setOnAction(event -> {
            int n;
            n = SelectedModel.deleteImage();
            if (n > 0) {
                controller.getSnackbar().enqueue(
                        new JFXSnackbar.SnackbarEvent("Successfully deleted " +
                                n +
                                " pictures"));
                if (displayWindowController != null) {
                    try {
                        displayWindowController.showNextImg();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                controller.getSnackbar().enqueue(
                        new JFXSnackbar.SnackbarEvent("failed to delete"));
            }
            homeController.sortAndRefreshImagesList(homeController.getSortComboBox().getValue());
            this.close();
        });
    }

    private void makeRenameDialog() {
        setBodyTextField();
        rightButton.setOnAction(event -> {
            if (SelectedModel.renameImage(bodyTextField.getText()))
                controller.getSnackbar().enqueue(
                        new JFXSnackbar.SnackbarEvent("Renamed successfully"));
            this.close();
            homeController.sortAndRefreshImagesList(homeController.getSortComboBox().getValue());
        });
    }

    private void makeInfoDialog() {
        rightButton.getStyleClass().add("dialog-confirm");
        rightButton.setText("Confirm");
    }

    private void makeReplaceDialog() {
        leftButton.setText("Jump Over");
        rightButton.setText("Replace");
        rightButton.setStyle("-fx-text-fill: BLUE;");
        leftButton.setOnAction(event -> {
            this.close();
            controller.getSnackbar().enqueue(
                    new JFXSnackbar.SnackbarEvent("Choose skip"));
            homeController.sortAndRefreshImagesList(
                    homeController.getSortComboBox().getValue());
        });
        rightButton.setOnAction(event -> {
            if (SelectedModel.replaceImage()) {
                controller.getSnackbar().enqueue(
                        new JFXSnackbar.SnackbarEvent("Replaced successfully"));
            }
            homeController.sortAndRefreshImagesList(
                    homeController.getSortComboBox().getValue());
            this.close();
            SelectedModel.setHavePastedNum(SelectedModel.getHavePastedNum() + 1);
        });
    }

}
