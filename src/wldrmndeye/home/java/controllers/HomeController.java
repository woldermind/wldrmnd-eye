package wldrmndeye.home.java.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXTextField;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.home.java.components.*;
import wldrmndeye.home.java.model.*;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.ResourceBundle;
import java.util.Stack;

/**
 * Controller of main window interface
 *
 * @see wldrmndeye.home.java.controllers.AbstractController
 */

public class HomeController 
        extends AbstractController implements Initializable {

    private static final Logger LOG =
            LoggerFactory.getLogger(HomeController.class);
    
    @FXML
    @Getter
    public JFXButton pasteButton;
    @FXML
    public JFXTextField searchTextField;
    @FXML
    public JFXButton closeSearchButton;
    @FXML
    public JFXButton gotoButton;
    @FXML
    public AnchorPane anchorPane;
    @FXML
    public JFXButton selectAllButton;
    @FXML
    @Getter
    private JFXButton refreshButton;

    @FXML
    private ToolBar infoBar;

    @FXML
    private Label folderInfoLabel;
    @FXML
    public Label selectedNumLabel;

    @FXML
    @Getter
    private JFXComboBox<String> sortComboBox;
    @Getter
    @Setter
    private boolean comboBoxClicked = false;
    @FXML
    private JFXTextField pathTextField;
    @Getter
    private JFXSnackbar snackbar;

    @FXML
    @Getter
    private StackPane rootPane;
    @FXML
    private FlowPane imageListPane = new FlowPane();
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private AnchorPane folderPane;

    private String currentPath = "";
    @Getter
    private Stack<String> pathStack1 = new Stack<>();
    @Getter
    private Stack<String> pathStack2 = new Stack<>();

    private ArrayList<ImageModel> curImgList = new ArrayList<>();

    private final static Character WINDOWS_SLASH_TYPE = '\\';
    private final static Character UNIX_SLASH_TYPE = '/';

    public HomeController() {
        // Add instances of this class to the global map
        ControllerUtil.controllers.put(this.getClass().getSimpleName(), this);
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imageListPane.setPadding(new Insets(10));
        imageListPane.setVgap(10);
        imageListPane.setHgap(10);
        imageListPane.setCache(true);
        SplitPane.setResizableWithParent(folderPane, false);

        snackbar = new JFXSnackbar(rootPane);
        infoBar.setBackground(Background.EMPTY);
        closeSearchButton.setVisible(false);

        initPasteButton();
        initSortComboBox();
        // The welcome page must be set after scrollPane,
        // otherwise it will be covered by the imageListPane blank page
        initIntroPage();
        initSearchTextField();
        initPathTextField();
        LOG.info("Main window initialization...");
    }
    
    /**
     * Generate and place image groups in the panel.
     * A thumbnail unit {@link ImageBox} contains:
     * An image ImageView (wrapped by {@link WhiteRippler} to achieve the ripple effect)
     * and a label {@link ImageLabel}
     */
    public void placeImages(ArrayList<ImageModel> imageModelList, String folderPath) {
        if (imageModelList == null)
            return;

        resetSettingBeforeGeneration();

        // Set the number of initial loading, you need to change the initial
        // index value in the scroll when changing
        int firstLoad = Math.min(imageModelList.size(), 80);
        // Modified the firstLoad value to be the minimum value between the list and 80

        updateCurrentAddress(folderPath);

        if (imageModelList.isEmpty()) {
            folderInfoLabel.setText("No pictures there");
            return;
        } else {
            int numberOfImagesInFolder =
                    ImageListModel.getNumberOfImagesInFolder(imageModelList);
            String imagesSize =
                    ImageListModel.getImagesSize(imageModelList);
            folderInfoLabel.setText(
                    String.format(
                            "%d pictures of %s",
                            numberOfImagesInFolder,
                            imagesSize));
            selectedNumLabel.setText("| 0 sheets selected");
        }
        
        int i;
        for (i = 0; i < firstLoad; i++) {
            ImageBox imageBox = new ImageBox(imageModelList.get(i));
            imageListPane.getChildren().add(imageBox);
        }

        imageListPane.setOnScroll(
                scrollEvent(imageModelList, firstLoad));
    }

    private EventHandler<ScrollEvent> scrollEvent(
            ArrayList<ImageModel> imageModelList, int firstLoad) {
        return new EventHandler<ScrollEvent>() {
            int positionAfterInitialLoad = firstLoad - 1;

            @Override
            public void handle(ScrollEvent event) {
                int numberOfPhotosPerScroll = 30;
                while (numberOfPhotosPerScroll > 0) {
                    positionAfterInitialLoad++;
                    if (isImageModelListLarger(event)) {
                        ImageBox imageBox = new ImageBox(
                                imageModelList.get(positionAfterInitialLoad));
                        imageListPane.getChildren().add(imageBox);
                    } else {
                        break;
                    }
                    numberOfPhotosPerScroll--;
                }

            }

            private boolean isImageModelListLarger(ScrollEvent event) {
                return event.getDeltaY() <= 0 &&
                        positionAfterInitialLoad < imageModelList.size();
            }
        };
    }

    private void resetSettingBeforeGeneration() {
        imageListPane.getChildren().clear();
        scrollPane.setContent(imageListPane);
        SelectionModel.clear();
        unSelectAll();
        sortComboBox.setVisible(true);
    }

    private void updateCurrentAddress(String folderPath) {
        pathTextField.setText(folderPath);
        currentPath = folderPath;
    }

    public void sortAndRefreshImagesList(String sort) {
        SelectionModel.clear();
        SelectedModel.getSourcePathList().clear();
        curImgList = ImageListModel.refreshList(currentPath, sort);
        placeImages(curImgList, currentPath);
    }

    public void initEnterFolder(String path) {
        currentPath = path;
        if (pathStack1.isEmpty() || !pathStack1.peek().equals(path)) {
            pathStack1.push(path);
            pathStack2.clear();
        }
        placeImages(ImageListModel.refreshList(currentPath), currentPath);
    }


    // The following are some initialization operations

    @FXML
    private void initIntroPage() {
        ImageView welcomeImage = new ImageView(
                new Image("/wldrmndeye/" +
                        "home/resources/images/intro.png"));
        welcomeImage.setFitWidth(825);
        welcomeImage.setPreserveRatio(true);
        HBox hBox = new HBox(welcomeImage);
        hBox.setAlignment(Pos.CENTER);
        StackPane stackPane = new StackPane(hBox);
        scrollPane.setContent(stackPane);
        LOG.info("Intro page show");
    }

    public void showNotFoundPage() {
        ImageView img = new ImageView(
                new Image("/wldrmndeye/" +
                        "home/resources/images/no_result.png"));
        img.setFitHeight(500);
        img.setPreserveRatio(true);
        HBox hBox = new HBox(img);
        hBox.setAlignment(Pos.CENTER);
        StackPane stackPane = new StackPane(hBox);
        scrollPane.setContent(stackPane);
        LOG.warn("Not found page show");
    }

    private void initSortComboBox() {
        sortComboBox.getItems().addAll(
                SortParam.SBNR,
                SortParam.SBND,
                SortParam.SBSR,
                SortParam.SBSD,
                SortParam.SBDR,
                SortParam.SBDD);
        sortComboBox.getSelectionModel().selectedItemProperty().addListener(
                        (observable, oldValue, newValue) -> {
            if (newValue != null) {
                sortAndRefreshImagesList(newValue);
                if (!comboBoxClicked)
                    setComboBoxClicked(true);
            }
        });
    }

    private void initSearchTextField() {
        searchTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER)
                searchImage();
        });
    }

    private void initPathTextField() {
        pathTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER)
                gotoPath();
        });
    }

    private void initPasteButton() {
        if (SelectedModel.getSourcePath() == null ||
                SelectedModel.getCopyOrMove() == -1) {
            pasteButton.setDisable(true);
        }
    }


    // Each button event operation

    @FXML
    private void moveBack() {
        if (!pathStack1.isEmpty()) {
            if (pathStack1.peek().equals(currentPath)) {
                pathStack1.pop();
            }
            String path;
            try {
                path = pathStack1.pop();
            } catch (EmptyStackException e) {
                LOG.error("Can't move back anymore");
                return;
            }
            pathStack2.push(currentPath);
            placeImages(ImageListModel.refreshList(path), path);
        } else {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("Back to the end"));
        }
    }

    @FXML
    private void moveForward() {
        if (!pathStack2.isEmpty()) {
            String path = pathStack2.pop();
            pathStack1.push(currentPath);
            placeImages(ImageListModel.refreshList(path), path);
        } else {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("Go to the end"));
        }
    }

    @FXML
    private void toParentDir() {
        try {
            toParentDir(WINDOWS_SLASH_TYPE);
            LOG.info("Windows slash type have been detected..");
        } catch (StringIndexOutOfBoundsException e) {
            LOG.info("Unix slash type have been detected");
            toParentDir(UNIX_SLASH_TYPE);
        }
    }

    private void toParentDir(char slashType) throws StringIndexOutOfBoundsException {
        String parent;
        if (currentPath.lastIndexOf(slashType) == 2) {
            if (currentPath.length() == 3) {
                snackbar.enqueue(
                        new JFXSnackbar.SnackbarEvent("Reach the root directory"));
                return;
            } else {
                parent = currentPath.substring(
                        0,
                        currentPath.lastIndexOf(slashType) + 1);
            }
        } else {
            try {
                parent = currentPath.substring(
                        0,
                        currentPath.lastIndexOf(slashType));
            } catch (StringIndexOutOfBoundsException e) {
                LOG.warn("Looks like unix-system delimeter reached..");
                return;
            }
        }
        placeImages(ImageListModel.refreshList(parent), parent);
        pathStack1.push(parent);
    }



    @FXML
    private void gotoPath() {
        String path = pathTextField.getText();

        if (path.endsWith(WINDOWS_SLASH_TYPE.toString()) && path.length() != 3)
            path = path.substring(0, path.length() - 1);

        File directory = new File(path);
        if (!directory.exists()) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("Incorrect path"));
            LOG.warn("Incorrect path");
        } else {
            ArrayList<ImageModel> list = ImageListModel.refreshList(path);

            if (list != null)
                placeImages(list, path);
        }
    }

    @FXML
    private void refresh() {
        unSelectAll();
        closeSearch();
        sortAndRefreshImagesList(sortComboBox.getValue());
        snackbar.enqueue(new JFXSnackbar.SnackbarEvent("Refreshed"));
        LOG.info("Image list refresh");
    }

    @FXML
    private void paste() {
        SelectedModel.pasteImage(currentPath);
        if (SelectedModel.getHavePastedNum() == SelectedModel.getWaitingPasteNum()) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("Paste successfully"));
            sortAndRefreshImagesList(sortComboBox.getValue());
            LOG.info("Paste success");
        }
        if (SelectedModel.getSourcePath() == null ||
                SelectedModel.getCopyOrMove() == -1) {
            pasteButton.setDisable(true);
        }
    }

    @FXML
    private void searchImage() {
        String key = searchTextField.getText();
        if (isSearchPathEmpty(key)) return;

        ArrayList<ImageModel> result =
                SearchImageModel.fuzzySearch(key, curImgList);
        placeImages(result, currentPath);
        if (result.size() == 0) {
            folderInfoLabel.setText("Picture not found");
            showNotFoundPage();
            LOG.warn("Picture not found");
        } else {
            folderInfoLabel.setText("Found " + result.size() + " results");
        }
        closeSearchButton.setVisible(true);
    }

    private boolean isSearchPathEmpty(String key) {
        return key.isEmpty();
    }

    @FXML
    private void closeSearch() {
        closeSearchButton.setVisible(false);
        searchTextField.setText("");
        sortAndRefreshImagesList(sortComboBox.getValue());
    }

    @FXML
    private void selectAll() {
        if (isNoOptionalContent()) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent("No content available"));
            LOG.warn("No content to select available");
            return;
        }
        SelectionModel.clear();
        for (Node node : imageListPane.getChildren()) {
            ImageBox imageBox = (ImageBox) node;
            imageBox.getCheckBox().setSelected(true);
        }
    }

    private boolean isNoOptionalContent() {
        return imageListPane.getChildren().isEmpty();
    }

    @FXML
    private void unSelectAll() {
        SelectionModel.clear();
    }
    
    @FXML
    private void showAboutDetail() {
        VBox vBox = new VBox();

        Label me = new Label(
                "Made for intechcore.com by\n" +
                "wldrmnd (Egor Barkovsky)\n\n");
        me.getStyleClass().add("normal-text-b");
        me.setTextAlignment(TextAlignment.CENTER);
        
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(me);

        CustomDialog dialog = new CustomDialog(this, 
                DialogType.INFO, 
                null, 
                "wldrmnd:eye");
        dialog.setBodyContent(vBox);
        dialog.show();
    }

}
