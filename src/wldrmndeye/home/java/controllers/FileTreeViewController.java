package wldrmndeye.home.java.controllers;

import com.jfoenix.controls.JFXTreeView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.home.java.model.SortParam;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * File directory tree display on the left
 */
public class FileTreeViewController implements Initializable {

    private static final Logger LOG =
            LoggerFactory.getLogger(FileTreeViewController.class);
    
    @FXML
    private JFXTreeView<File> fileTreeView;

    private HomeController homeController;

    public FileTreeViewController() {
        //Add instances of this class to the global map
        ControllerUtil.controllers.put(this.getClass().getSimpleName(), this);
        homeController = (HomeController) ControllerUtil.controllers.get(
                HomeController.class.getSimpleName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setFileTreeView();
    }

    private void setFileTreeView() {
        File[] rootList = File.listRoots();
        TreeItem<File> mainTreeItem = new TreeItem<>(rootList[0]);

        for (File root : rootList) {
            TreeItem<File> rootItem = new TreeItem<>(root);
            addItems(rootItem, 0);
            mainTreeItem.getChildren().add(rootItem);
        }

        fileTreeView.setRoot(mainTreeItem);
        fileTreeView.setShowRoot(false);

        // Custom cell, set folder picture
        fileTreeView.setCellFactory(new Callback<TreeView<File>, TreeCell<File>>() {
            @Override
            public TreeCell<File> call(TreeView<File> param) {
                TreeCell<File> treeCell = new TreeCell<File>() {
                    @Override
                    protected void updateItem(File item, boolean empty) {

                        if (!empty) {
                            super.updateItem(item, empty);
                            HBox hBox = new HBox();
                            //Determine the root directory separately to define the text
                            Label label = new Label(isListRoots(item));
                            this.setGraphic(hBox);

                            if (this.getTreeItem().isExpanded()) {
                                ImageView folderImage = new ImageView(
                                        "wldrmndeye/" +
                                                "home/resources/icons/opened_folder.png");
                                folderImage.setPreserveRatio(true);
                                folderImage.setFitWidth(22);
                                hBox.getChildren().add(folderImage);
                                this.setGraphic(hBox);
                            } else if (!this.getTreeItem().isExpanded()) {
                                ImageView folderImage = new ImageView(
                                        "wldrmndeye/" +
                                                "home/resources/icons/folder.png");
                                folderImage.setPreserveRatio(true);
                                folderImage.setFitWidth(22);
                                hBox.getChildren().add(folderImage);
                                this.setGraphic(hBox);
                            }
                            hBox.getChildren().add(label);
                        } else if (empty) {
                            this.setGraphic(null);
                        }
                    }
                };
                return treeCell;
            }
        });

        getClickOperationAndModeRefresh();
    }

    private void getClickOperationAndModeRefresh() {
        fileTreeView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    String absolutePath = newValue.getValue().getAbsolutePath();
                    LOG.info(absolutePath);
                    homeController.initEnterFolder(absolutePath);
                    homeController.getSortComboBox().setValue(SortParam.SBNR);
                    addItems(newValue, 0);
                });
    }

    public void addItems(TreeItem<File> in, int flag) {
        File[] files = in.getValue().listFiles();
        if (files != null) {
            if (flag == 0) {
                in.getChildren().remove(0, in.getChildren().size());
            }
            if (files.length > 0) {
                for (File file : files) {
                    if (file.isDirectory()&!file.isHidden()) {
                        TreeItem<File> newItem = new TreeItem<>(file);
                        if (flag < 1) {
                            addItems(newItem, flag + 1);
                        }
                        in.getChildren().add(newItem);
                    }
                }
            }
        }
    }
    
    public String isListRoots(File item) {
        File[] rootList = File.listRoots();
        for (File isListRoots : rootList) {
            if (item.toString().equals(isListRoots.toString())) {
                return item.toString();
            }
        }
        return item.getName();
    }
}
