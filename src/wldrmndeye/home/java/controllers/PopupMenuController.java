package wldrmndeye.home.java.controllers;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXSnackbar;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.display.java.controllers.DisplayWindowController;
import wldrmndeye.home.java.components.CustomDialog;
import wldrmndeye.home.java.components.DialogType;
import wldrmndeye.home.java.components.ImageBox;
import wldrmndeye.home.java.model.GenUtilModel;
import wldrmndeye.home.java.model.ImageModel;
import wldrmndeye.home.java.model.SelectedModel;
import wldrmndeye.home.java.model.SelectionModel;
import wldrmndeye.splice.SplicePreviewWindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Right-click menu controller for thumbnails
 *
 * @see wldrmndeye.home.java.controllers.AbstractController
 */

public class PopupMenuController implements Initializable {

    private static final Logger LOG =
            LoggerFactory.getLogger(PopupMenuController.class);

    @FXML
    private JFXListView<?> popupList;

    private ImageModel imageModel;
    private ImageBox imageBox;
    private HomeController homeController;

    @Getter
    private JFXSnackbar snackbar;

    public PopupMenuController() {
        //Add instances of this class to the global map
        ControllerUtil.controllers.put(this.getClass().getSimpleName(), this);
        homeController = (HomeController) ControllerUtil.controllers.get(
                HomeController.class.getSimpleName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        snackbar = new JFXSnackbar(homeController.getRootPane());
    }

    public PopupMenuController(ImageBox imageBox) {
        this();
        this.imageBox = imageBox;
        this.imageModel = imageBox.getImageModel();
    }

    @FXML
    private void action() {
        ArrayList<ImageModel> sourceList = SelectionModel.getSelectedImagesStore();
        switch (popupList.getSelectionModel().getSelectedIndex()) {
            case 0:
                copy(sourceList);
                break;
            case 1:
                cut(sourceList);
                break;
            case 2:
                rename(sourceList);
                break;
            case 3:
                compress(sourceList);
                break;
            case 4:
                splice(sourceList);
                break;
            case 5:
                remove(sourceList);
                break;
            case 6:
                info(sourceList);
                break;
            default:
        }
    }

    @FXML
    public void info(ArrayList<ImageModel> sourceList) {
        Image image = new Image(imageModel.getImageFile().toURI().toString());
        StringBuilder info = new StringBuilder();

        if (sourceList.isEmpty() || sourceList.size() == 1) {
            info = DisplayWindowController.appendedImageInfo(image, imageModel);
            new CustomDialog(homeController, DialogType.INFO, imageModel,
                    imageModel.getImageName(), info.toString()).show();
        } else {
            info.append("Quantity：").append(sourceList.size())
                    .append("\n");
            long totalSize = 0;
            for (ImageModel im : sourceList) {
                totalSize += im.getFileLength();
            }
            info.append("Size：")
                    .append(GenUtilModel.getFormatSize(totalSize))
                    .append("\n");
            info.append("Position：").append(imageModel.getImageParentPath())
                    .append("\n");
            CustomDialog dialog = new CustomDialog(
                    homeController,
                    DialogType.INFO, 
                    null,
                    "Multiple files",
                    info.toString());
            dialog.getBodyTextArea().setPrefHeight(150);
            dialog.show();
        }

        imageBox.getPopUpMenu().hide();
    }

    @FXML
    public void remove(ArrayList<ImageModel> sourceList) {
        if (sourceList.isEmpty()) {
            SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
            new CustomDialog(homeController, DialogType.DELETE, imageModel,
                    "Confirm deletion",
                    "File to be deleted：" + imageModel.getImageName() + " ?\n\n" +
                            "You can find it in the recycle bin :)").show();
        } else {
            SelectedModel.setSourcePath(sourceList);
            new CustomDialog(homeController, DialogType.DELETE, imageModel,
                    "Confirm deletion",
                    "Want to delete this" + sourceList.size() + "Files?\n\n" +
                            "You can find it in the recycle bin :)").show();
        }
        imageBox.getPopUpMenu().hide();
    }
    
    @FXML
    public void splice(ArrayList<ImageModel> sourceList) {
        if (sourceList.isEmpty() || sourceList.size() == 1) {
            snackbar.enqueue(
                    new JFXSnackbar.SnackbarEvent(
                            "Please select two or more pictures for stitching"));
        } else {
            SplicePreviewWindow previewWindow = new SplicePreviewWindow();
            previewWindow.initImageList(sourceList);
            try {
                previewWindow.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        imageBox.getPopUpMenu().hide();
    }

    @FXML
    public void compress(ArrayList<ImageModel> sourceList) {
        imageBox.getPopUpMenu().hide();

        if (sourceList.isEmpty()) {
            SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
        } else {
            SelectedModel.setSourcePath(sourceList);
        }
        int success = SelectedModel.compressImage(800);

        homeController.sortAndRefreshImagesList(homeController.getSortComboBox().getValue());
        if (success != 0) snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent(
                        "Compressed " + success +
                                " pictures and create a copy"));
        else snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent(
                        "No pictures to compress \n " +
                                "Compression conditions: greater than 800KB"));
    }

    @FXML
    public void rename(ArrayList<ImageModel> sourceList) {
        if (sourceList.isEmpty()) {
            SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
        } else {
            SelectedModel.setSourcePath(sourceList);
        }
        new CustomDialog(homeController, DialogType.RENAME, imageModel, "Rename picture").show();
        imageBox.getPopUpMenu().hide();
    }

    @FXML
    public void cut(ArrayList<ImageModel> sourceList) {
        if (sourceList.isEmpty()) {
            SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
            SelectedModel.setWaitingPasteNum(1);
        } else {
            SelectedModel.setSourcePath(sourceList);
            SelectedModel.setWaitingPasteNum(sourceList.size());
        }
        SelectedModel.setCopyOrMove(1);

        homeController.getPasteButton().setDisable(false);
        snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent("Cut to clipboard"));
        imageBox.getPopUpMenu().hide();
    }

    @FXML
    public void copy(ArrayList<ImageModel> sourceList) {
        if (sourceList.isEmpty()) {
            SelectedModel.setSourcePath(imageModel.getImageAbsoluteFilePath());
            SelectedModel.setWaitingPasteNum(1);
        } else {
            SelectedModel.setSourcePath(sourceList);
            SelectedModel.setWaitingPasteNum(sourceList.size());
        }
        SelectedModel.setCopyOrMove(0);

        homeController.getPasteButton().setDisable(false);
        snackbar.enqueue(
                new JFXSnackbar.SnackbarEvent("Copied to clipboard"));
        imageBox.getPopUpMenu().hide();
    }
}
