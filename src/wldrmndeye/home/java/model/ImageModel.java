package wldrmndeye.home.java.model;

import lombok.Data;

import java.io.File;

/**
 * Unit picture class
 **/

@Data
public class ImageModel {

    private String imageAbsoluteFilePath;
    private String imageParentPath;
    private File imageFile;
    private String imageName;
    private String imageNameWithoutExtension;
    private String imageType;
    private long fileLength;
    private long imageLastModificationTime;

    public ImageModel(File file) {
        this.imageFile = file;
        this.imageAbsoluteFilePath = file.getAbsolutePath();
        this.imageParentPath = file.getParent();
        this.imageName = file.getName();
        this.imageNameWithoutExtension = imageName.substring(
                0,
                imageName.lastIndexOf("."));
        this.imageType = imageName.substring(imageName.indexOf(".") + 1).toLowerCase();
        this.fileLength = file.length();
        this.imageLastModificationTime = file.lastModified();
    }

    public ImageModel(String path) {
        this.imageAbsoluteFilePath = path;
        this.imageFile = new File(path);
        this.imageParentPath = imageFile.getParent();
        this.imageName = imageFile.getName();
        this.imageNameWithoutExtension = imageName.substring(0, imageName.lastIndexOf("."));
        this.imageType = imageName.substring(imageName.indexOf(".") + 1).toLowerCase();
        this.fileLength = imageFile.length();
        this.imageLastModificationTime = imageFile.lastModified();
    }

    public String getFormatSize() {
        return GenUtilModel.getFormatSize(this.fileLength);
    }

    public String getFormatTime() {
        return GenUtilModel.getFormatTime(this.imageLastModificationTime);
    }
}
