package wldrmndeye.home.java.model;

import java.util.Comparator;

public class SortByDateComparatorImpl implements Comparator<ImageModel> {

    
    
    @Override
    public int compare(ImageModel firstImage, ImageModel secondImage) {
        return Long.compare(
                firstImage.getImageLastModificationTime(), 
                secondImage.getImageLastModificationTime());
    }
}
