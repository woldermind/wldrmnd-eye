package wldrmndeye.home.java.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 1. File screening
 * 2. Count the number of pictures
 * 3. Create a list of pictures
 * 4. Calculate the size of all pictures in the folder
 **/

@Data
public class ImageListModel {

    private static final Logger LOG =
            LoggerFactory.getLogger(ImageListModel.class);

    private static final String JPG_EXTENSION = ".jpg";
    private static final String JPEG_EXTENSION = ".jpeg";
    private static final String PNG_EXTENSION = ".png";
    private static final String GIF_EXTENSION = ".gif";
    private static final String BMP_EXTENSION = ".bmp";

    public static boolean isSupportedImg(String fileName) {
        return fileName.endsWith(JPG_EXTENSION) ||
                fileName.endsWith(JPEG_EXTENSION) ||
                fileName.endsWith(PNG_EXTENSION) ||
                fileName.endsWith(GIF_EXTENSION) ||
                fileName.endsWith(BMP_EXTENSION);
    }

    public static ArrayList<ImageModel> initImageList(String path) throws IOException {
        ArrayList<ImageModel> imageList = new ArrayList<>();
        if (path.isEmpty() || path == null) return imageList;

        Files.walkFileTree(
                Paths.get(path),
                getDefaultSimpleFileVisitor(path, imageList));
        return imageList;
    }

    private static SimpleFileVisitor<Path> getDefaultSimpleFileVisitor(
            String path,
            ArrayList<ImageModel> imageList) {
        return new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                String fileName = file.getFileName().toString().toLowerCase();
                if (isSupportedImg(fileName)) {
                    imageList.add(new ImageModel(file.toString()));
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                if (dir.toString().equals(path)) {
                    return FileVisitResult.CONTINUE;
                } else
                    return FileVisitResult.SKIP_SUBTREE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                return FileVisitResult.SKIP_SUBTREE;
            }
        };
    }

    public static int getNumberOfImagesInFolder(ArrayList<ImageModel> im) {
        return im.size();
    }

    public static String getImagesSize(ArrayList<ImageModel> imageModels) {
        long totalSize = 0;
        for (ImageModel imageModel : imageModels) {
            totalSize += imageModel.getFileLength();
        }
        return GenUtilModel.getFormatSize(totalSize);
    }

    public static ArrayList<ImageModel> refreshList(String path) {
        ArrayList<ImageModel> list = new ArrayList<>();
        try {
            list = initImageList(path);
        } catch (IOException | NullPointerException e) {
            LOG.error("Image list initialization failed");
        }
        return list;
    }

    public static ArrayList<ImageModel> refreshList(String path, String mode) {
        ArrayList<ImageModel> list = refreshList(path);
        if (list.isEmpty()) {
            LOG.warn("Image list is empty, nothing to refresh");
            return new ArrayList<>();
        }
        switch (mode) {
            case SortParam.SBND:
                assert list != null;
                Collections.reverse(list);
                LOG.info("Descending by Name sort");
                return list;
            case SortParam.SBSR:
                assert list != null;
                list.sort(new SortBySizeComparatorImpl());
                LOG.info("Ascending by Size sort");
                return list;
            case SortParam.SBSD:
                assert list != null;
                list.sort(new SortBySizeComparatorImpl());
                Collections.reverse(list);
                LOG.info("Descending by Size sort");
                return list;
            case SortParam.SBDR:
                assert list != null;
                list.sort(new SortByDateComparatorImpl());
                LOG.info("Ascending by Date sort");
                return list;
            case SortParam.SBDD:
                assert list != null;
                list.sort(new SortByDateComparatorImpl());
                Collections.reverse(list);
                LOG.info("Descending by Date sort");
                return list;
        }
        return list;
    }
}