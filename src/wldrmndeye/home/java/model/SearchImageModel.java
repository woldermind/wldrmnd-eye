package wldrmndeye.home.java.model;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchImageModel {

    private static final Logger LOG =
            LoggerFactory.getLogger(SearchImageModel.class);
    @Getter
    private static int numberOfPicturesFound = 0;

    // Implement fuzzy search and return a list of pictures for display,
    // it is recommended to use this method
    public static ArrayList<ImageModel> fuzzySearch(String name, ArrayList<ImageModel> imageModelList) {
        // Insensitive to case, if sensitive, remove CASE_INSENSITIVE
        Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);
        ArrayList<ImageModel> result = new ArrayList<>();
        for (ImageModel im : imageModelList) {
            Matcher matcher = pattern.matcher(im.getImageName());
            if (matcher.find()) {
                numberOfPicturesFound++;
                result.add(im);
            }
        }
        LOG.info("Fuzzy (recommended) search...");
        return result;
    }

    // To achieve precise search and return a result, the full name of the file
    // including the suffix is required
    public static ImageModel accurateSearch(String name, ArrayList<ImageModel> imageModelList) {
        // Insensitive to case, if sensitive, remove CASE_INSENSITIVE
        Pattern pattern = Pattern.compile(name, Pattern.UNIX_LINES);
        for (ImageModel im : imageModelList) {
            Matcher matcher = pattern.matcher(im.getImageName());
            if (matcher.matches()) {
                numberOfPicturesFound += 1;
                return im;
            }
        }
        LOG.info("Accurate search...");
        return null;
    }
}
