package wldrmndeye.home.java.model;

public final class SortParam {
    
    public static final String SBNR = "Ascending by name";
    public static final String SBND = "Descending by name";
    public static final String SBSR = "Ascending by size";
    public static final String SBSD = "Descending by size";
    public static final String SBDR = "Ascending order by modification date";
    public static final String SBDD = "Descending order by modified date";
    
}
