package wldrmndeye.home.java.model;

import com.jfoenix.controls.JFXSnackbar;
import com.sun.jna.platform.FileUtils;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.home.java.components.CustomDialog;
import wldrmndeye.home.java.components.DialogType;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.controllers.HomeController;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Objects;


/**
 * Selected image manipulation class
 * 1. Initialize source
 * 2. Copy
 * 3. Cut
 * 4. Paste
 * 5. Rename
 * 6. Delete
 * 7. Compress
 **/

public class SelectedModel {

    private static final Logger LOG =
            LoggerFactory.getLogger(SelectedModel.class);

    /**
     * Copy: If you encounter file duplication ->
     * 1. If the source folder is the same as the destination folder, rename
     * 2. If it is in a different folder, you can choose to overwrite or skip
     * Cut: if you encounter file duplication -> overwrite directly
     * Rename: if you encounter duplicate files -> overwrite directly
     */
    @Getter
    private static Path sourcePath;
    @Getter
    private static ArrayList<Path> sourcePathList = new ArrayList<>();
    private static Path targetPath;

    @Setter
    @Getter
    // Select Copy/Cut
    // 0->Copy 1->Cut
    private static int copyOrMove = -1;

    @Getter
    @Setter
    // Select single/multiple choice
    // 0->single choice 1->multiple choice
    private static int singleOrMultiple = -1;

    @Getter
    @Setter
    private static int waitingPasteNum = 0;
    @Getter
    @Setter
    private static int havePastedNum = 0;
    private static int coverImage = 0;

    private static HomeController homeController =
            (HomeController) ControllerUtil.controllers.get("HomeController");

    /**
     * Initialize source, copy/cut/rename/delete/compress option call
     */
    public static boolean setSourcePath(@NonNull ImageModel im) {
        sourcePath = im.getImageFile().toPath();
        singleOrMultiple = 0;
        return true;
    }

    public static boolean setSourcePath(@NonNull File f) {
        sourcePath = f.toPath();
        singleOrMultiple = 0;
        return true;
    }

    /**
     * When single selection, pass in a picture address and singleOrMultiple=0
     */
    public static boolean setSourcePath(String imagePath) {
        sourcePath = new File(imagePath).toPath();
        singleOrMultiple = 0;
        return true;
    }

    /**
     * When multiple selections are made, you can directly pass in a list at
     * the same time singleOrMultiple=1
     */
    public static boolean setSourcePath(ArrayList<ImageModel> imList) {
        sourcePathList.clear();
        for (ImageModel im : imList) {
            setSourcePath(im);
            sourcePathList.add(sourcePath);
        }
        singleOrMultiple = 1;
        return true;
    }

    /**
     * Paste option
     * 1. If the source folder is the same as the destination folder, rename
     * 2. If it is in a different folder, you can choose to overwrite or skip
     */
    public static boolean pasteImage(String path) {
        havePastedNum = 0;
        coverImage = 0;
        if (singleOrMultiple == 0) {
            try {
                microPaste(path);
            } catch (IOException e) {
                LOG.error("Paste failed");
                return false;
            }
        } else if (singleOrMultiple == 1) {
            try {
                for (Path p : sourcePathList) {
                    sourcePath = p;
                    microPaste(path);
                }
            } catch (IOException e) {
                LOG.error("Paste failed");
                return false;
            }
        }
        if (coverImage != 0) {
            homeController.getSnackbar().enqueue(
                    new JFXSnackbar.SnackbarEvent(
                            "Covered " + coverImage + " pictures"));
            LOG.info("Covered" + coverImage + " pictures successfully");
        }
        homeController.sortAndRefreshImagesList(
                homeController.getSortComboBox().getValue());
        return true;
    }

    public static boolean replaceImage() {
        try {
            Files.copy(sourcePath,
                    targetPath,
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LOG.error("Replacement failed!");
            return false;
        }
        return true;
    }

    private static void microPaste(String path) throws IOException {
        if (copyOrMove == 0) {
            if (getBeforePath().equals(path)) {
                boolean flag = false;
                String[] files = new File(path).list();
                String sourceFileName = sourcePath.getFileName().toString();
                for (String s : files) {
                    if (sourceFileName.equals(s) & !flag) {
                        targetPath = new File(suffixName(path, "_copy")).toPath();
                        flag = true;
                    }
                }
                if (!flag) {
                    targetPath = new File(otherPath(path)).toPath();
                }
                Files.copy(sourcePath, targetPath);
                havePastedNum++;
            } else {
                targetPath = new File(otherPath(path)).toPath();
                if (!imageRepeat(path)) {
                    Files.copy(sourcePath, targetPath);
                    havePastedNum++;
                } else {
                    show();
                }
            }
        } else if (copyOrMove == 1) {
            targetPath = new File(otherPath(path)).toPath();
            if (imageRepeat(path))
                coverImage++;
            Files.move(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
            havePastedNum++;
            if (havePastedNum == waitingPasteNum)
                copyOrMove = -1;
        }
    }

    /**
     * Need to judge whether there are duplicate pictures before pasting
     */
    private static boolean imageRepeat(String path) {
        String targetImageName = targetPath.getFileName().toString();
        try {
            if (isDuplicatedImagesFound(path, targetImageName)) {
                    return true;
            }
        } catch (IOException e) {
            LOG.error("Image duplication search failed");
            e.printStackTrace();
        }
        return false;
    }

    private static boolean isDuplicatedImagesFound(
            String path,
            String targetImageName) throws IOException {
        return SearchImageModel.accurateSearch(
                targetImageName,
                Objects.requireNonNull(ImageListModel.initImageList(path))) != null;
    }

    private synchronized static void show() {
        ImageModel im = new ImageModel(targetPath.toFile());
        new CustomDialog(homeController, DialogType.REPLACE, im,
                "Replace or skip files",
                "\nThe target already contains a\"" +
                        im.getImageName() +
                        "\"document\n")
                .show();
    }

    /**
     * Rename option, repeat naming directly overwrite
     */
    public static boolean renameImage(String newName) {
        if (singleOrMultiple == 0) {
            try {
                microRename(newName);
            } catch (IOException e) {
                LOG.error("Rename failed");
                return false;
            }
        } else if (singleOrMultiple == 1) {
            Path[] imArray = new Path[sourcePathList.size()];
            sourcePathList.toArray(imArray);
            for (int i = 0; i < imArray.length; i++) {
                sourcePath = imArray[i];
                try {
                    String beforeName = newName.substring(
                            0,
                            newName.lastIndexOf("."));
                    String afterName = newName.substring(
                            newName.lastIndexOf("."));
                    microRename(beforeName +
                            String.format("_%04d", i + 1) +
                            afterName);
                } catch (IOException e) {
                    LOG.error("Rename failed");
                    return false;
                }
            }
        }
        singleOrMultiple = -1;
        return true;
    }

    private static void microRename(String newName) throws IOException {
        targetPath = new File(otherName(newName)).toPath();
        Files.move(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Delete picture option
     *
     * @return the number of successfully deleted pictures
     */
    public static int deleteImage() {
        int success = 0;
        if (singleOrMultiple == 0) {
            try {
                microDelete();
                success++;
            } catch (IOException e) {
                LOG.error("Failed to delete");
                return 0;
            }
        } else if (singleOrMultiple == 1) {
            for (Path p : sourcePathList) {
                sourcePath = p;
                try {
                    microDelete();
                    success++;
                } catch (IOException e) {
                    LOG.error("Failed to delete");
                    return 0;
                }
            }
        }
        singleOrMultiple = -1;
        return success;
    }

    private static void microDelete() throws IOException {
        FileUtils fileUtils = FileUtils.getInstance();
        if (fileUtils.hasTrash()) {
            try {
                fileUtils.moveToTrash(new File[]{(sourcePath.toFile())});
            } catch (UnsatisfiedLinkError | NoClassDefFoundError e) {
                LOG.warn("Looks like unix-system defender do not " +
                         "accept this movement");
            }
        }
    }
    
    public static int compressImage(int desSize) {
        if (singleOrMultiple == 0) {
            try {
                if (!microCompress(desSize))
                    return 0;
            } catch (IOException e) {
                LOG.error("Failed to compress");
                return 0;
            }
            return 1;
        } else if (singleOrMultiple == 1) {
            int success = 0;
            for (Path p : sourcePathList) {
                sourcePath = p;
                try {
                    if (microCompress(desSize))
                        success++;
                } catch (IOException e) {
                    LOG.error("Failed to compress");
                    return 0;
                }
            }
            return success;
        }
        singleOrMultiple = -1;
        return 0;
    }

    private static boolean microCompress(int desSize) throws IOException {
        byte[] imageBytes = GenUtilModel.getByteByFile(sourcePath.toFile());
        if (isNoNeedToCompress(desSize, imageBytes)) {
            return false;
        }
        double accuracy;
        if (imageBytes.length > desSize * 1024) {
            accuracy = getAccuracy(imageBytes.length / 1024.0);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(imageBytes.length);
            Thumbnails.of(sourcePath.toFile())
                    .scale(accuracy)
                    .outputQuality(accuracy)
                    .toOutputStream(bos);
//                        .toFile(newFile);  // Slightly slower :(
            imageBytes = bos.toByteArray();
        }
        String newImagePath = suffixName(getBeforePath(), "_only");
        File newFile = new File(newImagePath);
        return GenUtilModel.getFileByByte(imageBytes, newFile);
    }

    private static boolean isNoNeedToCompress(int desSize, byte[] imageBytes) {
        return imageBytes == null || imageBytes.length < desSize * 1024;
    }

    private static double getAccuracy(double imageSize) {
        double accuracy;
        if (imageSize < 1024 * 2) {
            accuracy = 0.71;
        } else if (imageSize < 1024 * 4) {
            accuracy = 0.66;
        } else if (imageSize < 1024 * 8) {
            accuracy = 0.61;
        } else {
            accuracy = 0.59;
        }
        return accuracy;
    }
    
    private static String checkPath(String path) {
        StringBuilder sb = new StringBuilder(32);
        if (!path.endsWith("\\")) {
            sb.append(path).append("\\");
        } else {
            sb.append(path);
        }
        return sb.toString();
    }

    private static String getBeforePath() {
        String path = sourcePath.toString();
        String beforePath = "";
        try {
            beforePath = path.substring(0, path.lastIndexOf("\\"));
        } catch (StringIndexOutOfBoundsException e) {
            LOG.error("Can't produce that type of path");
        }
        return beforePath;
    }

    private static String otherPath(String newPath) {
        StringBuilder otherPath = new StringBuilder(32);
        otherPath
                .append(checkPath(newPath))
                .append(sourcePath.getFileName().toString());
        return otherPath.toString();
    }

    private static String otherName(String newName) {
        StringBuilder otherName = new StringBuilder(32);
        try {
            getRenamedPath(newName, otherName, "\\");
        } catch (StringIndexOutOfBoundsException e) {
            LOG.warn("Unix system is used there!");
            getRenamedPath(newName, otherName, "/");
        }
        return otherName.toString();
    }

    private static void getRenamedPath(String newName,
                                       StringBuilder otherName,
                                       String s) {
        String path = sourcePath.toString()
                .substring(
                        0,
                        sourcePath.toString().lastIndexOf(s));
        otherName
                .append(path)
                .append(s)
                .append(newName);
    }

    private static String suffixName(String newPath, String suffix) {
        StringBuilder suffixName = new StringBuilder(32);
        String sourceName = sourcePath.getFileName().toString();
        String nameBefore = sourceName.substring(0, sourceName.indexOf("."));
        String nameAfter = sourceName.substring(sourceName.indexOf("."));
        suffixName
                .append(checkPath(newPath))
                .append(nameBefore)
                .append(suffix)
                .append(nameAfter);
        return suffixName.toString();
    }
}
