package wldrmndeye.home.java.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GenUtilModel {

    private static final Logger LOG =
            LoggerFactory.getLogger(GenUtilModel.class);

    private static final double KB = 1024.0;
    private static final double MB = 1024.0 * 1024.0;
    private static final double GB = 1024.0 * 1024.0 * 1024.0;

    public static String getFormatSize(long fileLength) {
        String defaultSize;
        if (fileLength < KB){
            defaultSize = String.format("%d Byte", fileLength);
        } else if (fileLength < MB){
            defaultSize = String.format("%.0f KB", fileLength/KB);
        } else if (fileLength < GB){
            defaultSize = String.format("%.2f MB", fileLength/MB);
        } else {
            defaultSize = String.format("%.2f GB", fileLength/GB);
        }
        return defaultSize;
    }

    public static String getFormatTime(long time) {
        Date data = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return sdf.format(data);
    }

    static byte[] getByteByFile(File file) {
        try (FileInputStream fis = new FileInputStream(file);
             ByteArrayOutputStream bos = new ByteArrayOutputStream(1024)) {
            byte[] bytes = new byte[1024];
            int i;
            while ((i = fis.read(bytes)) != -1) {
                bos.write(bytes, 0, i);
            }
            return bos.toByteArray();
            } catch (IOException e) {
            LOG.error("Can't get byte[] array from image file");
            e.printStackTrace();
        }
        return null;
    }

    static boolean getFileByByte(byte[] bytes, File file) {
        try (FileOutputStream fos = new FileOutputStream(file);
             BufferedOutputStream bos = new BufferedOutputStream(fos)){
            bos.write(bytes);
            return true;
        } catch (IOException e) {
            LOG.error("Can't get file from byte[] array");
            e.printStackTrace();
        }
        return false;
    }
}
