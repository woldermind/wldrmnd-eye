package wldrmndeye.home.java.model;

import com.jfoenix.effects.JFXDepthManager;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wldrmndeye.home.java.components.ImageBox;
import wldrmndeye.home.java.controllers.ControllerUtil;
import wldrmndeye.home.java.controllers.HomeController;

import java.util.ArrayList;

/**
 * Store the tool class of the selected picture.
 * Contains a list {@link ArrayList}, and some changes
 * to the display status of the selected pictures
 */
public class SelectionModel {

    private static final Logger LOG =
            LoggerFactory.getLogger(SelectionModel.class);

    private static ArrayList<ImageBox> selectedThumbnailUnitStore =
            new ArrayList<>();

    @Getter
    public static ArrayList<ImageModel> selectedImagesStore =
            new ArrayList<>();

    private static HomeController homeController =
            (HomeController) ControllerUtil.controllers.get(
                    HomeController.class.getSimpleName());

    public static void add(ImageBox node) {
        JFXDepthManager.setDepth(node, 4);
        node.getEyeImageView().setTranslateY(
                node.getEyeImageView().getTranslateY() - 5);
        selectedThumbnailUnitStore.add(node);
        selectedImagesStore.add(node.getImageModel());
        homeController.selectedNumLabel.setText(
                "| Selected " + selectedThumbnailUnitStore.size());
        log();
    }

    public static void remove(ImageBox node) {
        JFXDepthManager.setDepth(node, 0);
        node.getEyeImageView().setTranslateY(
                node.getEyeImageView().getTranslateY() + 5);
        selectedThumbnailUnitStore.remove(node);
        selectedImagesStore.remove(node.getImageModel());
        homeController.selectedNumLabel.setText(
                "| Selected " + selectedThumbnailUnitStore.size());
        log();
    }

    public static void clear() {
        while (!selectedThumbnailUnitStore.isEmpty()) {
            selectedThumbnailUnitStore.iterator().next()
                    .getCheckBox().setSelected(false);
        }
    }

    private static void log() {
        LOG.info("Items in list: " + selectedThumbnailUnitStore);
    }
}
