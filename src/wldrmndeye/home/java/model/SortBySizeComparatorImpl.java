package wldrmndeye.home.java.model;

import java.util.Comparator;

public class SortBySizeComparatorImpl implements Comparator<ImageModel> {
    
    @Override
    public int compare(ImageModel first, ImageModel second) {
        return Long.compare(
                first.getFileLength(), 
                second.getFileLength());
    }
}
