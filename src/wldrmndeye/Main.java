package wldrmndeye;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends Application {

    private static final Logger LOG =
            LoggerFactory.getLogger(Main.class);
    private static final String LOG4J_CONFIG_PATH = "src/wldrmndeye/home/resources/log4j.properties";


    @Override
    public void start(Stage primaryStage) throws Exception {
        PropertyConfigurator.configure(LOG4J_CONFIG_PATH);

        primaryStage.setTitle("wldmrnd:eye");

        //Adjust the length and width adaptively according to the screen size
        double width = 800;
        double height = 600;

        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            width = bounds.getWidth() / 1.45;
            height = bounds.getHeight() / 1.35;
        } catch (Exception e){
            e.printStackTrace();
        }
        Parent root = FXMLLoader.load(getClass().getResource(
                "/wldrmndeye/" +
                        "home/resources/fxml/Home.fxml"));
        Scene scene = new Scene(new JFXDecorator(primaryStage, root), width, height);

        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(Main.class.getResource(
                "/wldrmndeye/home/resources/css/home.css")
                .toExternalForm());

        primaryStage.getIcons().add(
                new Image(
                        Main.class.getResourceAsStream(
                                "/wldrmndeye/home/resources/icons/app.png")));
        primaryStage.setScene(scene);
        primaryStage.show();
        LOG.info("Starting wldrmnd:eye...");
    }


    public static void main(String[] args) {
        launch(args);
    }
}
